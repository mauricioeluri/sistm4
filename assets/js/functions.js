//LOGIN
$(document).ready(function () {
    $("button[name=btn_login]").click(function (e) {
        $email = $("input[name=email]");
        $senha = $("input[name=senha]");
        $("#resLogin").html("<p>Autenticando...</p>");
        e.preventDefault();
        $.post("login", {email: $email.val(), senha: $senha.val()},
                function (retorno) {
                    if (retorno === "") {
                        window.location = "";
                    }
                    $("#resLogin").html(retorno);
                }
        );
    });
});