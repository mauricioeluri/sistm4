<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('session.cache_limiter', 'public');
session_cache_limiter(FALSE);

// VARIAVEIS SEÇÃO -> secVar      --ex-->   secEmail, secNome

class Home extends CI_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->library(array('session'));
        $this->load->model('m_usuario');
        $this->load->helper(array('form', 'utility'));
        $this->load->library('form_validation');
    }

    public function index() {
        if (isset($_SESSION['secEmail'])) {
            $variaveis['usuarios'] = $this->m_usuario->get();
            $this->load->view('v_home', $variaveis);
            return TRUE;
        }
        redirect('home/login');
    }

    public function login() {
        if (isset($_SESSION['secEmail'])) {
            redirect('home');
        }
        $variaveis = new stdClass();

        if ($this->input->is_ajax_request()) {
            //VALIDACOES
            $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');
            $this->form_validation->set_rules('senha', 'Senha', 'required');

            if ($this->form_validation->run() == TRUE) {
                //$variaveis['resultado'] = password_hash("123", PASSWORD_DEFAULT);
                $email = $this->input->post('email');
                $senha = $this->input->post('senha');
                $resolveLogin = $this->m_usuario->verificaLogin($email, $senha);
                $variaveis->mensagem = '<p>Erro ao efetuar o login. Dados incorretos!</p>';
                if ($resolveLogin == TRUE) {
                    $id = $this->m_usuario->getIdByEmail($email);
                    $user = $this->m_usuario->getUser($id);
                    $this->setSession($user);
                    $variaveis->mensagem = '';
                    return TRUE;
                }
            }
            $this->load->view('errors/v_novoUsuario', $variaveis);
            return FALSE;
        }
        $this->load->view('v_login', $variaveis);
        $this->load->view('errors/v_novoUsuario', $variaveis);
    }

    public function sair() {
        $this->session->sess_destroy();
        redirect('home/login');
    }

    public function adicionar_usuario() {
        if (isset($_SESSION['secEmail'])) {
            if ($this->input->is_ajax_request()) {
                $variaveis = new stdClass();
                //VALIDAÇÕES
                $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|max_length[255]|is_unique[usuarios.email]');
                $this->form_validation->set_rules('senha', 'Senha', 'required|min_length[6]|max_length[255]');
                $this->form_validation->set_rules('texto', 'Texto', 'required');

                $this->form_validation->set_message('min_length', 'O campo %s deve ter pelo menos %s caractere(s).');
                $this->form_validation->set_message('max_length', 'O campo %s deve ter no máximo %s caractere(s).');

                if ($this->form_validation->run() == TRUE) {
                    $novoUsr = array(
                        'email' => $this->input->post('email'),
                        'senha' => $this->input->post('senha'),
                        'texto' => $this->input->post('texto')
                    );
                    $variaveis->mensagem = "<p>Houve um erro ao salvar os dados. Por favor, tente novamente.</p>";
                    $insereUsuario = $this->m_usuario->insereUsuario($novoUsr);
                    if ($insereUsuario == TRUE) {
                        $variaveis->mensagem = "<p>Gravado com sucesso!</p>";
                        //redireciona
                    }
                }
                $this->load->view('errors/v_novoUsuario', $variaveis);
                return FALSE;
            }
        }
        redirect('home/login');
    }

    public function atualiza($id = NULL) {
        if (isset($_SESSION['secEmail'])) {
            if ($this->input->is_ajax_request()) {
                $variaveis = new stdClass();
                $valida = '';
                if ($this->input->post('emailOrig') != $this->input->post('email')) {
                    $valida = '|is_unique[usuarios.email]';
                }
                $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email|max_length[255]' . $valida);
                $this->form_validation->set_rules('texto', 'Texto', 'required');
                $this->form_validation->set_message('max_length', 'O campo %s deve ter no máximo %s caractere(s).');

                if ($this->form_validation->run() == TRUE) {
                    $novoUsr = array(
                        'id' => $this->input->post('id'),
                        'email' => $this->input->post('email'),
                        'texto' => $this->input->post('texto')
                    );
                    $insereUsuario = $this->m_usuario->attUsuario($novoUsr);
                    if ($insereUsuario == FALSE) {
                        $variaveis->mensagem = "<p>Houve um erro ao salvar os dados. Por favor, tente novamente.</p>";
                        $this->load->view('errors/v_novoUsuario', $variaveis);
                        return FALSE;
                    }
                    return TRUE;
                }
                $this->load->view('errors/v_novoUsuario', $variaveis);
                return FALSE;
            }
            if (!$id) {
                redirect();
            }
            $user = $this->m_usuario->getUser($id);
            if ($user == NULL) {
                return FALSE;
            }
            $variaveis = array(
                'usrId' => $id,
                'usrEmail' => (string) $user->email,
                'usrTexto' => (string) $user->texto
            );
            $this->load->view('v_atualizar', $variaveis);
            return FALSE;
        }
        redirect('home/login');
    }

    public function deleta($id = NULL) {
        if (isset($_SESSION['secEmail'])) {
            $this->m_usuario->delete($id);
            $variaveis['usuarios'] = $this->m_usuario->get();
            $this->load->view('v_tabelaUsuarios', $variaveis);
        }
    }

    public function burro() {
        echo "<h1 style='font-weight:bold;text-align:center;font-size:150px;'>COMO VOCÊ É BURRO</h1>";
    }

    private function setSession($user) {
        $_SESSION = array(
            'secId' => (int) $user->id,
            'secEmail' => (string) $user->email,
            'secTexto' => (string) $user->texto
        );
        return TRUE;
    }

}
