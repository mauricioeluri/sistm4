<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_usuario extends CI_Model {

    public function verificaLogin($email, $senha) {
        if ($email && $senha) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
                return FALSE;
            }
            $this->db->where('email', $email);
            $qr = $this->db->get('usuarios');
            foreach ($qr->result_array() as $row) {
                $hash = $row['senha'];
                if (password_verify($senha, $hash)) {
                    return TRUE;
                }
            }
            return FALSE;
        }
        return FALSE;
    }

    public function getIdByEmail($email) {
        if ($email) {
            $this->db->from('usuarios');
            $this->db->where('email', $email);
            return $this->db->get()->row('id');
        }
        return FALSE;
    }

    public function getUser($id) {
        if ($id) {
            $this->db->from('usuarios');
            $this->db->where('id', $id);
            return $this->db->get()->row();
        }
        return FALSE;
    }

    public function insereUsuario($novoUsr) {
        if ($novoUsr) {
            $dados = array(
                'email' => $novoUsr['email'],
                'senha' => password_hash($novoUsr['senha'], PASSWORD_BCRYPT),
                'texto' => $novoUsr['texto']
            );
            return $this->db->insert('usuarios', $dados);
        }
        return FALSE;
    }

    public function attUsuario($usr) {
        if ($usr) {
            $dados = array(
                //'id' => $usr['id'],
                'email' => $usr['email'],
                'texto' => $usr['texto']
            );
            $this->db->where('id', $usr['id']);
            return $this->db->update("usuarios", $dados);
        }
        return FALSE;
    }

    public function get() {
        $this->db->order_by("id", 'desc');
        return $this->db->get('usuarios');
    }

    public function delete($id = NULL) {
        if ($id) {
            return $this->db->where('id', $id)->delete('usuarios');
        }
        return FALSE;
    }

}
