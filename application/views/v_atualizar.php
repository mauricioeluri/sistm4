<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?= asset_js("jquery-2.2.1.min") ?>

<script>
    $(document).ready(function () {
        $("button[name=btn_att]").click(function (e) {
            $("#resultado_att").html("<p>Validando...</p>");
            $id = $("input[name=usrId]");
            $email = $("input[name=email]");
            $emailOrig = $("input[name=emailOrig]");
            $texto = $("input[name=texto]");
            e.preventDefault();
            $.post("home/atualiza", {id: $id.val(), email: $email.val(), emailOrig: $emailOrig.val(), texto: $texto.val()},
                    function (retorno) {
                        if (retorno === "") {
                            window.location = "../../";
                        }
                        $("#resultado_att").html(retorno);
                    }
            );
        });
    });
</script>
<style>
    .red{
        color: red;
    }
</style>
<?= form_open(base_url('home/atualiza')) ?>
<h3>Atualizar usuário:</h3>
<input type="hidden" name='usrId' value="<?= $usrId ?>"/>
<input type="hidden" name='emailOrig' value="<?= $usrEmail ?>"/>
<input type="text" name="email" placeholder="E-mail" value="<?= $usrEmail ?>" autofocus required/>
<input type="textarea" name="texto" placeholder="Descreva brevemente o usuário." value="<?= $usrTexto ?>" required/>
<button type="submit" name="btn_att">Entrar</button>
</form>
<div class="red" id="resultado_att"></div>

