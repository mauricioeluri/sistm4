<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Esqueleto Sistema com Ajax</title>
        <?= asset_js("jquery-2.2.1.min") ?>
        <?= asset_js("functions") ?>
        <style>
            .red{color:#FF0000}
        </style>        
    </head>
    <body>
        <div id="resultado"><p>O texto do seu usuário é: <?= $_SESSION['secTexto'] ?></p></div>
        <?= anchor('home/sair', 'Sair') ?>
        <br><br>
        <div>
            <?php $this->load->view('v_adicionar'); ?>
        </div>
        <br>
        <div id="tabela">
            <?php $this->load->view('v_tabelaUsuarios'); ?>
        </div>
    </body>
</html>
