<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script>
    $(document).ready(function () {
        $("button[name=btn_adicionar]").click(function (e) {
            $("#resultado_adc").html("<p>Validando...</p>");
            $email = $("input[name=email]");
            $senha = $("input[name=senha]");
            $texto = $("input[name=texto]");
            e.preventDefault();
            $.post("home/adicionar_usuario", {email: $email.val(), senha: $senha.val(), texto: $texto.val()},
                    function (retorno) {
                        $("#resultado_adc").html(retorno);
                        $.get({
                            url: "home/deleta",
                            success: function (retorno2) {
                                $("#tabela").html(retorno2);
                            }
                        });
                    }
            );
        });
    });
</script>

<?= form_open(base_url('home/adicionar_usuario/#')) ?>
<h3>Adicionar usuário:</h3>
<input type="text" name="email" placeholder="E-mail" autofocus required/>
<input type="password" name="senha" placeholder="Senha" required/>
<input type="textarea" name="texto" placeholder="Descreva brevemente o usuário." required/>
<button type="submit" name="btn_adicionar">Entrar</button>
</form>
<div class="red" id="resultado_adc"></div>