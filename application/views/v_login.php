<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login não mais Dinâmico Ajax</title>
        <style>
            .red{color:#FF0000}
        </style>
        <?= asset_js("jquery-2.2.1.min") ?>
        <?= asset_js("functions") ?>
    </head>
    <body>
        <?= form_open(base_url()) ?>
        <h3>Efetuar login:</h3>
        <input type="text" name="email" placeholder="usuario" autofocus required/>
        <input type="password" name="senha" placeholder="Senha" required/>
        <button type="submit" name="btn_login">Entrar</button>
        <?= anchor("home/burro", 'Esqueci minha senha') ?>
        <br><small>mauricio@gmail.com - 123</small>
    </form>
    <div class="red" id="resLogin">