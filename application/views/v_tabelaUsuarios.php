<?php
defined('BASEPATH') OR exit('No direct script access allowed');
echo '<h3>'.$usuarios->num_rows().' registro(s)</h3>';
if ($usuarios->num_rows() > 0) {
    ?>
    <table border="1" style="text-align: center">
        <thead>
            <tr>
                <th>Id</th>
                <th>E-mail</th>
                <th>Texto</th>
                <th>Remover</th>
                <th>Atualizar</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuarios->result() as $usuario): ?>
                <tr id="tabela">
                    <td><?= $usuario->id ?></td>
                    <td><?= $usuario->email ?></td>
                    <td><?= $usuario->texto ?></td>
                    <td><a href ="" class="removeUsu" id="<?= $usuario->id ?>">X</a></td>
                    <td><a href="home/atualiza/<?= $usuario->id ?>">Atualizar</a></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <script>
        $('.removeUsu').click(function () {
            if (confirm("Você tem certeza que deseja remover o usuário?")) {
                id = $(this).attr("id");
                $.get({
                    url: "home/deleta/" + id,
                    success: function (retorno) {
                        $("#tabela").html(retorno);
                    }
                });
            }
        });
    </script>
<?php }else { ?>
    <h4>Nenhum registro cadastrado.</h4>
    <?php
}